## WHAT'S THAT

Teeman is just a bash tool to manage your Teeworlds server(s).

Just add your server configuration file(s) in the ~/teeman/configuration directory.  
You must name them xxx.conf  
All the server configuration files called xxx.conf will be processed if you run teeman [command] !  
The first time you will execute teeman, it will set its directory and download Teeworlds, it will take few minutes, do not worry :)  
Also, teeman can work with MODs using handlers, see the HANDLERS section. Please note, that teeman works better with official Teeworlds game.   
See the HOW TO section for further details.

## INSTALLATION

You can use git if it's setup on your server  
    
    git clone https://florianleleu@bitbucket.org/florianleleu/teeman.git

or just download

    wget --no-check-certificate -O teeman.tar.gz https://bitbucket.org/florianleleu/teeman/get/master.tar.gz
    tar xzvf teeman.tar.gz  

then
	
	cd teeman
	chmod +x teeman
	
Finally move the file teeman in /usr/bin (requires to be root), this is quite handy, you just have to type teeman to execute it.  
Else you can leave it where it is but you'll have to call it like this ./teeman

Voilà !

## HOW TO USE IT

- Help 

        teeman help

- Start servers  

        teeman start

- Start a specific server  

        teeman start myconfiguration
		
- Stop servers  

        teeman stop

- Stop a specific server  

        teeman stop myconfiguration

- Restart servers  

        teeman restart

- Restart a specific server  

        teeman restart myconfiguration
		
- Get status of servers  

        teeman status

- Get status of a specific server  

        teeman status myconfiguration
		
- Get the list of all your configurations  

        teeman list
		
## HANDLERS (how to deal with MODs)

To use MODs with teeman it's quite "simple". It uses a system called handlers.  
A handler is a way to tell teeman with which executable file you will start your configuration.  
It's based on a pattern in the name of your configuration.  
An example is better rather than some blabla.  

The handlers configuration file is ~/teeman/handlers

    # rule name ; prefix ; binary file
    # race ; race (ex : race.my_configuration.conf) ; race_srv 
    default;;teeworlds_srv
	
Let's say you want to use a MOD, for example DDRace.  
You will add this rule in the handlers file this way :

    my ddrace rule;ddr;DDRace
	
* my ddrace rule  

    It's a name for you to remember, you can put whatever you want, but keep it simple and unique !

* ddr  

    It's the prefix you will put in your configuration name, it's how it will be matched (ex: ddr.my-conf.conf, ddr.my-conf2.conf)

* DDRace  

    It's the executable file you have to put in ~/teeman/teeworlds, don't forget to make it executable !   
    Obviously you have to download the executable for DDRace, it's not in teeman by default.  

With this new rule, all the server configuration files starting with ddr. will be executed by DDRace.  

Now create your configuration file, let's say ddr.my-conf.conf, ans set your directive (sv_name, etc ...)  

Finally, just start your configuration by typing in your console

    teeman start ddr.my-conf
	
or (which will start of the configurations found)

    teeman start
   
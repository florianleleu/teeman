#!/bin/bash

_DIR="${HOME}/teeman"
_CONFIGURATION_DIR="${_DIR}/configuration"
_LOG_DIR="${_DIR}/log"
_HANDLERS="${_DIR}/handlers"
_TEEWORLDS_DIR="${_DIR}/teeworlds"

function install () {
	echo -n "${_DIR} not found ! Installation is required ... "
	mkdir -p "${_DIR}" "${_CONFIGURATION_DIR}" "${_TEEWORLDS_DIR}" "${_LOG_DIR}" 2>/dev/null
	if [ ! "$?" -eq 0 ] ; then
		echo "failed"
		echo "${_DIR} cannot be created, do you have a home directory?"
		exit
	fi
	cd "${_DIR}"
	_arch=$([ "$(uname -m)" == "x86_64" ] && echo "x86_64" || echo "x86")
	_url="https://www.teeworlds.com/files/teeworlds-0.6.2-linux_${_arch}.tar.gz"
	_name=$(echo ${_url} | awk -F '/' '{print $NF}')
	wget -q -O "${_name}" --no-check-certificate "${_url}"	
	tar xzf "${_name}" && rm -f "${_name}"
	_name=$(echo "${_name}" | rev | cut -c 8- | rev)
	mv ${_name}/* "${_TEEWORLDS_DIR}"
	rm -Rf "${_name}"
	sed -i -e "s#^add_path \$USERDIR#add_path ${_TEEWORLDS_DIR}#g" "${_TEEWORLDS_DIR}/storage.cfg"
	echo "# rule name ; prefix ; binary file" >> ${_HANDLERS}
	echo "# race ; race (ex : race.my_configuration.conf) ; race_srv" >> ${_HANDLERS}
	echo "default;;teeworlds_srv" >> ${_HANDLERS}
	chown -R ${USER}:${USER} "${_DIR}" && chmod -R 755 "${_DIR}"
	echo "done."
	echo "You can now put your configuration file(s) in ${_CONFIGURATION_DIR}"
	exit 0
}

function usage () {
    echo "Usage: teeman [command] [arguments]"
    echo
    echo "Manage Teeworlds servers"
    echo
    echo "  teeman start <configuration>         - start the server <configuration>"
    echo "  teeman stop <configuration>          - stop the server <configuration>"
    echo "  teeman restart <configuration>       - restart the server <configuration>"
    echo "  teeman status <configuration>        - get the status of the server <configuration>"		
    echo "  teeman fullstatus <configuration>    - get the fullstatus of the server <configuration>"		
    echo "  teeman list                          - list all the configurations"		
    echo
    echo "If no specific configuration file is given the command applies to all configuration files."
	echo "Configuration directory: ${_CONFIGURATION_DIR}"
}

function convert_hexatimestamp_to_datetime () {
	if [ -z "$1" ] ; then
		return
	fi
	
	if [[ -n "$(echo "$1" | grep -E "^[0-9]{2,4}(-[0-9]{2}){2} ([0-9]{2}:){2}[0-9]{2}$")" ]] ; then
		date -d "$1" +'%F %T'
	else
		date -d @$(echo $((0x$1))) +'%F %T'	
	fi
}

function which_handler () {
	if [ -z "$1" ] ; then
		return
	fi

	if [ ! -f "${_HANDLERS}" ] ; then
		echo "${_HANDLERS} not found !"
		exit
	fi

	_handler=$(echo "$1" | awk -F '.' '{if(NF >= 2)print $1;}')	
	if [ -z "${_handler}" ] ; then
		grep -E "^default;" ${_HANDLERS} | awk -F ';' '{print $3}'
	else
		grep -v "^#" ${_HANDLERS} | grep -E ";\s*${_handler}\s*;" | awk -F ';' '{print $3}'		
	fi
}

function running () {
	if [ -z "$1" ] ; then
		return
	fi
	
	ps aux | grep "$1" | grep -v "grep" | head -1 | awk '{print $2}'
}

function start () {
	if [ -n "$1" ] ; then
		_conf="$1.conf"	
		if [ ! -f "${_CONFIGURATION_DIR}/${_conf}" ] ; then
			echo "$1 does not exist !"
			return
		fi
		
		_pid="$(running ${_conf})"
		if [ -n "${_pid}" ] ; then
			echo "$1 is ALREADY started (pid ${_pid})"
			return
		fi
		
		_log="${_LOG_DIR}/$1.log"
		if [ ! -f  "${_log}" ] ; then
			touch "${_log}" && chown "${USER}":"${USER}" "${_log}" && chmod 644 "${_log}"
		fi
		
		_handler=$(which_handler $1)		
		if [ -z "${_handler}" ] ; then
			echo "ERROR: no handler found for $1"
			return
		fi
		
		if [ ! -f "${_TEEWORLDS_DIR}/${_handler}" ] ; then
			echo "ERROR: handler ${_handler} for $1 cannot be found"
			return	
		fi
		
		cd ${_TEEWORLDS_DIR}
		_pid=$(./${_handler} -f ${_CONFIGURATION_DIR}/${_conf} &>>${_log} & echo $!)
		cd - &>/dev/null
		
		if [ "${_pid}" == "$(running ${_conf})" ] ; then
			echo "$1 started (pid ${_pid})"
		else
			echo "$1 is NOT started, you may check ${_log} for further informations"

			_error=$(grep -E "port [0-9]{1,5} might already be in use|failed to load map" "${_log}" | tail -1 | awk -F ':' '{print $2}' | sed -e "s/^\ *//g" -e "s/\ *$//g")
			if [ -n "${_error}" ] ; then
				echo "ERROR: ${_error}"
			fi	
		fi
	else
		for (( i=0 ; i < ${#_CONFIGURATIONS[@]} ; i++ ));
		do
			_conf="${_CONFIGURATIONS[${i}]}"
			start "${_conf%.*}"
		done
	fi
}

function stop () {
	if [ -n "$1" ] ; then
		_conf="$1.conf"		
		if [ ! -f "${_CONFIGURATION_DIR}/${_conf}" ] ; then
			echo "$1 does not exist !"
			return
		fi
		
		_pid=$(running ${_conf})
		if [ -n "${_pid}" ] ; then
			kill -9 "${_pid}"
			echo "$1 stopped (pid ${_pid})"	
		else
			echo "$1 is already NOT running"
		fi
	else
		for (( i=0 ; i < ${#_CONFIGURATIONS[@]} ; i++ ));
		do
			_conf="${_CONFIGURATIONS[${i}]}"
			stop "${_conf%.*}"
		done
	fi
}

function restart () {
	if [ -n "$1" ] ; then
		stop "$1"
		start "$1"
	else
		stop
		start
	fi
}

function status () {
	if [ -n "$1" ] ; then
		_conf="$1.conf"		
		if [ ! -f "${_CONFIGURATION_DIR}/${_conf}" ] ; then
			echo "$1 does not exist !"
			return
		fi
		
		_pid=$(running ${_conf})

		if [ -n "${_pid}" ] ; then
			echo "$1 is running (pid ${_pid})"
		else
			echo "$1 is NOT running"			
		fi
	else
		for (( i=0 ; i < ${#_CONFIGURATIONS[@]} ; i++ ));
		do
			_conf="${_CONFIGURATIONS[${i}]}"
			status "${_conf%.*}"
		done
	fi
}

function fullstatus () {
	if [ -n "$1" ]; then
		_conf="$1.conf"	
		if [ ! -f "${_CONFIGURATION_DIR}/${_conf}" ] ; then
			echo "$1 does not exist !"
			return
		fi
		
		_pid=$(running ${_conf})
		_state="NOT running"
		_port="undefined"
		_masterserver="undefined"
		_log="${_LOG_DIR}/$1.log"
		_log_size="undefined"
		_runtime="undefined"
		_first_start="undefined"
		_last_start="undefined"
		_current_players="0"
		_all_players="0"
		_gametype=$(grep "sv_gametype" "${_CONFIGURATION_DIR}/${_conf}" | awk '{print $2}' | tr '[:lower:]' '[:upper:]')
		if [ -z "${_gametype}" ] ; then
			_gametype="DM"
		fi		

		_handler=$(which_handler $1)		
		if [ -z "${_handler}" ] ; then
			_handler="no handler"
		fi
		
		if [ -f  "${_log}" ] ; then
			_log_size=$(du -sh ${_log} | awk '{print $1}')
			
			_t=$(grep "\[server\]: version" ${_log} | head -1 | awk -F ']' '{print $1}' | tr -d '[')
			if [ -n "${_t}" ] ; then
				_first_start=$(convert_hexatimestamp_to_datetime "${_t}")
			fi 
			
			_t=$(grep "\[server\]: version" ${_log} | tail -1 | awk -F ']' '{print $1}' | tr -d '[')
			if [ -n "${_t}" ] ; then
				_last_start=$(convert_hexatimestamp_to_datetime "${_t}")
			fi
			
			_all_players=$(grep 'leave player' ${_log} | awk -F "'" '{print $2}' | cut -d : -f2 | sort -u | wc -l)
		
			if [ -n "${_pid}" ] ; then
				_state="running (pid ${_pid})"			
				_port=$(netstat -tupln  2>/dev/null | grep -E "^udp +.*${_pid}" | awk -F ':' '{print $2}' | awk '{print $1}')
				_current_players=$(grep "Teams are balanced" ${_log} | tail -1 | awk -F '(' '{print $2}' | tr -d ')=[a-z]' | awk '{print $1+$2}')
				if [ -z "${_current_players}" ] ; then
					_current_players="0"
				fi				
				_now_timestamp=$(date +%s)
				_last_start_timestamp=$(date -d "${_last_start}" +'%s')
				_runtime="$((${_now_timestamp} - ${_last_start_timestamp}))s"
				
				if [ -z "${_runtime}" ] ; then
					_runtime="undefined"
				fi				
				
				if [ -z "$(grep -E "sv_register\s+0" "${_CONFIGURATION_DIR}/${_conf}")" ] ; then
					_masterserver=$(grep -E "\[register\]: chose 'master[0-9]\.teeworlds\.com'" "${_log}" | tail -1 | awk -F "'" '{print $2}')
					if [ -z "${_masterserver}" ] ; then
						_masterserver="undefined"
					fi
				else
					_masterserver="none (LAN)"
				fi
			fi		
		fi
		
		echo "name:              $1"
		echo "configuration:     ${_CONFIGURATION_DIR}/${_conf}"
		echo "log:               ${_log} (${_log_size})"
		echo "state:             ${_state}"
		echo "handler:           ${_handler}"
		echo "port:              ${_port}"
		echo "masterserver:      ${_masterserver}"
		echo "gametype:          ${_gametype}"
		echo "runtime:           ${_runtime}"
		echo "first start:       ${_first_start}"
		echo "last start:        ${_last_start}"
		echo "current players:   ${_current_players}"
		echo "all time players:  ${_all_players}"
	else
		for (( i=0 ; i < ${#_CONFIGURATIONS[@]} ; i++ ));
		do
			_conf="${_CONFIGURATIONS[${i}]}"
			fullstatus "${_conf%.*}"

			if [ ${i} -lt $((${#_CONFIGURATIONS[@]} - 1)) ] ; then
				echo
			fi
		done
	fi
}

function list () {
	for (( i=0 ; i < ${#_CONFIGURATIONS[@]} ; i++ ));
	do
		_conf="${_CONFIGURATIONS[${i}]}"
		echo "${_conf%.*}"
	done
}

if [ "$(id -u)" -eq 0 ] ; then
	echo "Do not use teeman as root, consider using an other user."
	exit
fi

if [ ! -d "${_DIR}" ] ; then
	install
fi

OLDIFS=$IFS
IFS=$'\n'
_CONFIGURATIONS=( $(ls -A ${_CONFIGURATION_DIR}/*.conf 2>/dev/null | awk -F '/' '{print $NF}') )
IFS=$OLDIFS

if [ -z "${_CONFIGURATIONS}" ] ; then
	echo "ERROR: no configuration file(s) found in ${_CONFIGURATION_DIR}"
	exit 1
fi

_command="$1"
shift

case "${_command}" in
	start)
		start "$1"
		;;
	stop|kill)
		stop "$1"
		;;
	restart)
		restart "$1"
		;;
	status)
		status "$1"
		;;
	fullstatus)
		fullstatus "$1"
		;;
	list)
		list
		;;
	help|*)
		usage
		;;
esac